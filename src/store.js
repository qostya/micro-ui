import { createStore, combineReducers } from 'redux';
import calcReducer from './reducers/calculationReducer';

export default createStore(
  combineReducers({
    calculationState: calcReducer
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);