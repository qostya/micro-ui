export function getRequest(url, params) {
  let queryParams;

  if (params) {
    queryParams = Object.keys(params).reduce((full, curr) => {
      if (params[curr] != undefined) {
        return (full !== '?' ? full + '&' : '?') + encodeURIComponent(curr) + '=' + encodeURIComponent(params[curr]);
      } else {
        return full;
      }
    }, '?')
  }


  return fetch(url + (params ? queryParams : ''), {
    method: 'GET'
  }).then(res => res.json());
}