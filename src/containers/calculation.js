import { h, Component } from 'preact';
import { connect } from 'preact-redux';

import CalculationComponent from '../components/calculationComponent';
import { getRequest } from '../api/api';
import { CALC_FIELD_CHANGED } from '../actions';

class Calculation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mark: null,
      model: null,
      exploitation_area: null
    };

    this.findCarMarkOptions = {
      clearOnEmpty: true,
      request: (value) => {
        return getRequest("https://enter.b2bpolis.ru/rest/v4/car-mark", {
          search: value
        });
      }
    };

    this.findCarModelOptions = {
      request: (value) => {
        return getRequest("https://enter.b2bpolis.ru/rest/v4/car-model", {
          search: value,
          car_mark: this.state.mark.id
        });
      }
    };

    this.findExploitationAreaOptions = {
      frontendFilter: true,
      request: (value) => {
        return getRequest("https://enter.b2bpolis.ru/rest/v4/owner-registration").then(function (response) {
          response.results = response.results.filter(item => {
            return item.title.toLowerCase().indexOf(value.toLowerCase()) !== -1;
          });

          return response;
        });
      }
    };
  }

  render() {
    return <CalculationComponent onFieldChange={this.props.onFieldChange}
                                 findCarMarkOptions={this.findCarMarkOptions}
                                 findExploitationAreaOptions={this.findExploitationAreaOptions}
                                 findCarModelOptions={this.findCarModelOptions}
                                 fields={this.props.calculationParams} />;
  }
}

function mapStateToProps(store) {
  return {
    calculationParams: store.calculationState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFieldChange(fieldName, value) {
      dispatch({
        type: CALC_FIELD_CHANGED,
        value,
        fieldName
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Calculation);