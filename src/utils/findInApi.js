import { h, Component } from 'preact';

import { debounce } from '../utils'

class FindInApi extends Component {
  constructor(props) {
    super(props);

    this.state = {
      foundItems: [],
      value: props.value
    };

    this.updateValue = function (event) {
      let value = event.target ? event.target.value : event;

      this.setState({
        value
      });

      return value;
    };

    function onFieldChange(value) {
      if (!value && props.options.clearOnEmpty) {
        return this.setState({
          foundItems: []
        });
      }

      return props.options.request(value).then(res => {
        this.setState({
          foundItems: res.results.length > 20 ? res.results.splice(0, 20) : res.results
        });
      });
    }

    this.onFieldChangeDebounced = debounce(onFieldChange.bind(this), props.debounceSpeed || 100);

    this.fieldChanged = function (event) {
      const value = this.updateValue(event);

      this.onFieldChangeDebounced(value);
    }.bind(this);
  }

  selectItem(item) {
    this.setState({
      foundItems: []
    });

    if (this.props.onChange) this.props.onChange(item);

    this.updateValue(item.title);
  }

  render(props) {
    return (
      <div className="m-icro__find">
        <input type="text"
               placeholder={this.props.placeholder}
               disabled={this.props.disabled}
               value={this.state.value}
               onInput={this.fieldChanged} />

        {this.state.foundItems.length > 0 ? (
            <div className="m-icro__find-results">
              {this.state.foundItems.map((item, index) => (
                <div className="m-icro__find-results-item"
                     onClick={this.selectItem.bind(this, item)}
                     key={index}>{item.title}</div>
              ))}
            </div>
          ) : undefined}
      </div>
    );
  }
}

export default FindInApi;