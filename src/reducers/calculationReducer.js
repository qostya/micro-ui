import extend from 'xtend';
import { CALC_FIELD_CHANGED } from '../actions';

const initialState = {
  calc_type: 'kasko',
  car_mark: null,
  car_model: null,
  car_cost: 500000,
  car_manufacturing_year: (new Date()).getFullYear()
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CALC_FIELD_CHANGED: {
      return extend({}, state, {
        [action.fieldName]: action.value
      });
    }
  }

  return state;
};