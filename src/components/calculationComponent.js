import { h } from 'preact';


import FindInApi from '../utils/findInApi';
import MaskComponent from './maskComponent';
import Numpad from './numpad';

export default (props) => {
  return (
    <div className="m-icro">
      <div className="m-icro__content m-icro__clearfix">
        <div className="m-icro__head">
          <div className="m-icro__title">
            Быстрый расчет КАСКО
          </div>
        </div>

        <div className="m-icro__body">
          <div className="m-icro__form">
            <div className="m-icro__block">
              <div className="m-icro__row m-icro__row--two">
                <div className="m-icro__column">
                  <div className="m-icro__field">
                    <div className="m-icro__label">
                      Марка
                    </div>

                    <div className="m-icro__textbox">
                      <FindInApi onChange={props.onFieldChange.bind(null, 'mark')}
                                 options={props.findCarMarkOptions}
                                 placeholder="Ford / Kia / др."/>
                    </div>
                  </div>
                </div>

                <div className="m-icro__column">
                  <div className="m-icro__field">
                    <div className="m-icro__label">
                      Модель
                    </div>

                    <div className="m-icro__textbox">
                      <FindInApi onChange={props.onFieldChange.bind(null, 'model')}
                                 options={props.findCarModelOptions}
                                 disabled={!props.fields.mark}
                                 placeholder="Веберите модель"/>
                    </div>
                  </div>
                </div>
              </div>



              <div className="m-icro__row m-icro__row--two">
                <div className="m-icro__column">
                  <div className="m-icro__field">
                    <div className="m-icro__label">
                      Стоимость
                    </div>

                    <div className="m-icro__textbox">
                      {/*<input type="text"*/}
                             {/*placeholder="500 000"/>*/}
                      <Numpad onChange={props.onFieldChange.bind(null, 'car_cost')}
                              placeHolder="В руб."
                              maskPattern="money"
                              value={props.fields.car_cost}>
                        <MaskComponent />
                      </Numpad>

                    </div>
                  </div>
                </div>

                <div className="m-icro__column">
                  <div className="m-icro__field">
                    <div className="m-icro__label">
                      Год выпуска
                    </div>

                    <div className="m-icro__textbox">
                      <MaskComponent maskPattern="9999"
                                     placeHolder="2017"
                                     value={props.fields.car_manufacturing_year}
                                     onChange={props.onFieldChange.bind(null, 'car_manufacturing_year')} />
                    </div>
                  </div>
                </div>
              </div>

              <div className="m-icro__row m-icro__row--two">
                <div className="m-icro__column">
                  <div className="m-icro__field">
                    <div className="m-icro__label">
                      Мощность
                    </div>

                    <div className="m-icro__textbox">
                      <MaskComponent maskPattern="999"
                                     placeHolder="л.с."
                                     value={props.fields.engine_power}
                                     onChange={props.onFieldChange.bind(null, 'engine_power')} />
                    </div>
                  </div>
                </div>

                <div className="m-icro__column">
                  <div className="m-icro__field">
                    <div className="m-icro__label">
                      Регион использ.
                    </div>

                    <div className="m-icro__textbox">
                      <FindInApi onChange={props.onFieldChange.bind(null, 'exploitation_area')}
                                 options={props.findExploitationAreaOptions}
                                 placeholder="Санкт-Петербург"/>
                    </div>
                  </div>
                </div>
              </div>

              <div className="m-icro__hr"></div>

              <div className="m-icro__persons">
                <div className="m-icro__person">
                  <div className="m-icro__person-title">
                    Водитель

                    <div className="m-icro__persons-add">+</div>
                  </div>

                  <div className="m-icro__row m-icro__row--two">
                    <div className="m-icro__column">
                      <div className="m-icro__field">
                        <div className="m-icro__label">
                          Возраст
                        </div>

                        <div className="m-icro__textbox">
                          <input type="text"
                                 placeholder="18"/>
                        </div>
                      </div>
                    </div>

                    <div className="m-icro__column">
                      <div className="m-icro__field">
                        <div className="m-icro__label">
                          Стаж
                        </div>

                        <div className="m-icro__textbox">
                          <input type="text"
                                 placeholder="0"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="m-icro__actions">
              <button className="m-icro__button m-icro__button--calculate"
                      type="button">Получить тарифы</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};