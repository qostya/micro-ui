import { h, Component } from 'preact';
import mask from 'vanilla-masker';


class MaskComponent extends Component {
  constructor(props) {
    super(props);

    this.updateValue = function (event) {
      let value = toPattern(event.target ? event.target.value : event);

      this.props.onChange(value);

      return value;
    }.bind(this);

    function toPattern(value) {
      if (props.maskPattern !== 'money') {
        return mask.toPattern(value, props.maskPattern);
      }

      return mask.toMoney(value, {
        precision: 0,
        delimiter: ' '
      });
    }

    this.toPattern = toPattern;
  }

  render() {
    const { value = 0, onChange, ...props } = this.props;
    const formattedValue = this.toPattern(value);

    return (
      <input {...props}
             value={formattedValue}
             placeholder={this.props.placeHolder}
             onInput={this.updateValue} />
    );
  }
}

export default MaskComponent;