import { h, Component } from 'preact';

class Numpad extends Component {
  constructor() {
    super();

    this.state = {
      isVisible: false,
    };
  }

  toggleNumpadVisibility(isVisible) {
    isVisible = isVisible === undefined ? !this.state.isVisible : isVisible;

    this.setState({
      isVisible: isVisible
    });
  }

  handleNumpadClick(value, event) {
    let currentValue = String(this.props.value);

    if (value === 'ok') {
      this.toggleNumpadVisibility(false);
      return;
    }

    if (value === '<') {
      this.props.onChange(Number(currentValue.substr(0, currentValue.length - 1)) || '');
      return;
    }

    this.props.onChange((Number(currentValue) || '') + (value + ''));
  }

  render() {
    const elements = [1, 2, 3, 4, 5, 6, 7, 8, 9, '<', 0, 'ok'];
    const Child = this.props.children[0].nodeName;
    const numpadClasses = `m-icro__numpad ${this.state.isVisible ? 'm-icro__numpad--active' : ''}`;

    return (
      <div className="m-icro__pos-rel">
        <Child onChange={this.props.onChange}
               value={this.props.value}
               onFocus={this.toggleNumpadVisibility.bind(this, true)}
               maskPattern={this.props.maskPattern}
               placeHolder={this.props.placeHolder} />

        <div className={numpadClasses}>
          <div className="m-icro__row m-icro__row--three">
            {elements.map(item => {
              return (
                <div className="m-icro__column m-icro__numpad-item"
                     onClick={this.handleNumpadClick.bind(this, item)}>
                  {item}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default Numpad;
