import { Provider, connect } from 'preact-redux';
import { h, render } from 'preact';

import store from './store';
import App from './app';

const Main = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

render(<Main />, document.getElementById('m-icro-app'));