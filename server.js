const http = require('http');
const sttck = require('node-static');
const file = new sttck.Server('./');

const port = 5544;

http.createServer((req, res) => {
  file.serve(req, res);
}).listen(port);